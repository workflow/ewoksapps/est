# CHANGELOG.md

## Unreleased

## 1.0.0

Changes:

- Add rebinning to the `Larch_autobk` task.
- Support concatenated multi-XAS scans that can be uni- or bi-directional.
- Support concatenated multi-XAS scans with fixed section size as well as monotonic section detection.

New Features:

- Ewoks task `SplitBlissScan` to split concatenated multi-XAS scan HDF5 data.
- `get_data_from_url` supports online data access.

## 0.7.2

Bug fixes:

- Fix icon reading

## 0.7.1

New Features:

- Add support for Python 3.12

## 0.7.0

New Features:

- Read bi-directional scan data with monotonic section detection.

Changes:

- Remove `pkg_resources` usage and related changes in Orange packages
- Refactor HDF5 and ASCII data reading
