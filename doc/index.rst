Summary
-------

`est` provides `ewoks tasks <https://ewoks.readthedocs.io/en/latest>`_ that can be connected
together to define a workflow for X-ray Absorption Structure analysis. Tasks are based on
either `PyMca <https://github.com/vasole/pymca>`_ or `Larch <https://xraypy.github.io/xraylarch/>`_.

An `Orange3 <https://github.com/biolab/orange3>`_ add-on is also provided by the library to
help user defining graphically the workflow they want to run.

.. image:: user_guide/img/workflow_example.png

.. toctree::
   :hidden:

   tutorials/index.rst
   user_guide/index.rst
   api.rst
   app/index.rst
   CHANGELOG.rst
