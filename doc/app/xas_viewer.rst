xas-viewer
==========

Purpose
-------

*est xas-viewer* is the command to launch a simple viewer of spectroscopy data

.. image:: img/xas_viewer.png
   :width: 600 px
   :align: center


Usage
-----

.. code-block:: bash

    est xas-viewer [[--input --input-spectra-url]]

Examples of usage
-----------------

.. code-block:: bash

    # from a simple ASCII file:
    est xas-viewer --input EXAFS_Cu.dat
    # from a hdf5 file using the '@' syntax
    est xas-viewer --input-spectra data@B33_test_1.h5 --input-channel energy@B33_test_1.h5
    # from a hdf5 file using silx DataUrl
    est xas-viewer --input-spectra "silx:///[...]B33_test_1.h5?path=/data"  --input-channel "silx:///[...]B33_test_1.h5?path=/energy"

