canvas
======

Purpose
-------

*est canvas* is the command to launch canvas used for defining workflows.
This is a proxy to *orange-canvas* command.

.. image:: img/canvas.png
   :width: 600 px
   :align: center


Usage
-----

::
    est canvas [[workflow-file]]

Examples of usage
-----------------

.. code-block:: bash

    est canvas my_workflow.ows
