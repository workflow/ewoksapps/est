Noise
=====

Display raw data noise using Savitsky-Golay.

.. image:: img/noise.png
