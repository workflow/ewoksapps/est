ROI definition
==============

Use to define a roi on the dataset.

.. image:: img/xas_roi.png
