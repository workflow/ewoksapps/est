xas output
==========

Use to dump configuration and spectra into a .h5 file

.. image:: img/xas_output.png
