# Est: Spectroscopy workflows

`est` is a library to define workflows for X-ray Absorption Structure analysis.
The analysis can be based on either [PyMca](https://github.com/vasole/pymca) or [Larch](https://xraypy.github.io/xraylarch/)

![An example workflow](https://ewoksest.readthedocs.io/en/latest/_images/workflow_example.png)

## Quick start

```python
pip install est[full] pyqt5
ewoks-canvas
```

See then the _Example workflows_ to get started.

## Documentation

https://ewoksest.readthedocs.io/
