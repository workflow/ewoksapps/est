"""Example data sources:

* EXAFS_Cu.dat: https://raw.githubusercontent.com/vasole/pymca/master/PyMca5/PyMcaData/EXAFS_Cu.dat
* EXAFS_Ge.dat: https://raw.githubusercontent.com/vasole/pymca/master/PyMca5/PyMcaData/EXAFS_Ge.dat
* cu_rt01.xmu: https://raw.githubusercontent.com/xraypy/xraylarch/master/examples/xafs/cu_rt01.xmu
"""
