NAME = "xas_pymca"

ID = "orangecontrib.est.widgets.pymca"

DESCRIPTION = "pymca orange widget"

LONG_DESCRIPTION = "pymca XAS processes"

ICON = "icons/pymca.png"

BACKGROUND = "light-green"

PRIORITY = 2
