#!/bin/bash
set -e

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ewoks execute ${SCRIPT_ROOT}/exafs.ows -p input_information="{\"spectra_url\": \"silx://${SCRIPT_ROOT}/cu_rt01.h5?/1.1/measurement/mu\", \"channel_url\": \"silx://${SCRIPT_ROOT}/cu_rt01.h5?/1.1/measurement/energy\", \"dimensions\": [2, 1, 0], \"energy_unit\": \"electron_volt\"}"

ewoks execute ${SCRIPT_ROOT}/exafs.ows -p input_information="{\"spectra_url\": \"spec://${SCRIPT_ROOT}/EXAFS_Cu.dat?1 cu.dat 1.1 Column 2/Column 2\", \"channel_url\": \"spec://${SCRIPT_ROOT}/EXAFS_Cu.dat?1 cu.dat 1.1 Column 2/Column 1\", \"energy_unit\": \"electron_volt\"}"

ewoks execute ${SCRIPT_ROOT}/exafs.ows -p input_information="{\"spectra_url\": \"larch://${SCRIPT_ROOT}/cu_rt01.xmu\", \"channel_url\": \"larch://${SCRIPT_ROOT}/cu_rt01.xmu\", \"energy_unit\": \"electron_volt\"}"

ewoks execute ${SCRIPT_ROOT}/exafs.ows -p input_information="{\"spectra_url\": \"ascii://${SCRIPT_ROOT}/EXAFS_Ge1.csv?/mu\", \"channel_url\": \"ascii://${SCRIPT_ROOT}/EXAFS_Ge1.csv?/energy\", \"energy_unit\": \"electron_volt\"}"

ewoks execute ${SCRIPT_ROOT}/exafs.ows -p input_information="{\"spectra_url\": \"ascii://${SCRIPT_ROOT}/EXAFS_Ge2.csv?/Column 2\", \"channel_url\": \"ascii://${SCRIPT_ROOT}/EXAFS_Ge2.csv?/Column 1\", \"energy_unit\": \"electron_volt\"}"

ewoks execute ${SCRIPT_ROOT}/exafs.ows -p input_information="{\"spectra_url\": \"ascii://${SCRIPT_ROOT}/EXAFS_Ge.dat?/Column 2\", \"channel_url\": \"ascii://${SCRIPT_ROOT}/EXAFS_Ge.dat?/Column 1\", \"energy_unit\": \"electron_volt\"}"
