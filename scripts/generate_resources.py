from urllib.request import urlopen
from pathlib import Path
from est import resources


def download(url: str, filename: Path):
    with urlopen(url) as response:
        with open(filename, "wb") as out_file:
            out_file.write(response.read())
    assert filename.exists()
    print("SAVED:", filename)


def download_larch_cu():
    url = "https://raw.githubusercontent.com/xraypy/xraylarch/master/examples/xafs/cu_rt01.xmu"
    with resources.resource_path("exafs", "cu_rt01.xmu") as filename:
        download(url, filename)
    resources.generate_resource("exafs", "cu_rt01.xmu", overwrite=True)


def download_pymca_cu():
    url = "https://raw.githubusercontent.com/vasole/pymca/master/PyMca5/PyMcaData/EXAFS_Cu.dat"
    with resources.resource_path("exafs", "EXAFS_Cu.dat") as filename:
        download(url, filename)
    resources.generate_resource("exafs", "EXAFS_Cu.dat", overwrite=True)


def download_pymca_ge():
    url = "https://raw.githubusercontent.com/vasole/pymca/master/PyMca5/PyMcaData/EXAFS_Ge.dat"
    with resources.resource_path("exafs", "EXAFS_Ge.dat") as filename:
        download(url, filename)
    resources.generate_resource("exafs", "EXAFS_Ge.dat", overwrite=True)


if __name__ == "__main__":
    download_larch_cu()
    download_pymca_cu()
    download_pymca_ge()
