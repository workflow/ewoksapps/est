from ewokscore import execute_graph
from ewoksutils.task_utils import task_inputs

if __name__ == "__main__":

    inputs = {
        "filename": "/data/scisoft/ewoks/ch7280/id24-dcm/20250131/RAW_DATA/Ru_WVC1/Ru_WVC1_1_RT_air/Ru_WVC1_1_RT_air.h5",
        "scan_number": 1,
        "monotonic_channel": "measurement/energy_enc",
        "out_filename": "/tmp/test.h5",
        "subscan_size": 3001,
    }

    node = {
        "task_type": "class",
        "task_identifier": "est.core.process.split.SplitBlissScan",
    }
    graph = {"graph": {"graph_version": "1.1", "id": "split"}, "nodes": [node]}
    print(execute_graph(graph, inputs=task_inputs(inputs=inputs)))
